package com.inteligo.quiz.bank.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="tb_change_cash")
public class Change implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="SEQ_CHANGE")
	@SequenceGenerator(name="SEQ_CHANGE", sequenceName = "SEQ_CHANGE", allocationSize=1)
	private int id;

	@Column(name = "id_client")
	private String idClient;

	@Column(name = "from_money")
	private String fromMoney;

	@Column(name = "to_money")
	private String toMoney;

	@Column(name = "exchange_rate")
	private String exchangeRate;

	@Column(name = "money")
	private String money;

	@Column(name = "created")
	private Date created;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getFromMoney() {
		return fromMoney;
	}

	public void setFromMoney(String fromMoney) {
		this.fromMoney = fromMoney;
	}

	public String getToMoney() {
		return toMoney;
	}

	public void setToMoney(String toMoney) {
		this.toMoney = toMoney;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "Change{" +
				"id=" + id +
				", idClient='" + idClient + '\'' +
				", fromMoney='" + fromMoney + '\'' +
				", toMoney='" + toMoney + '\'' +
				", exchangeRate='" + exchangeRate + '\'' +
				", money='" + money + '\'' +
				", created=" + created +
				'}';
	}

	public Change() {
	}
}