package com.inteligo.quiz.bank.controller;

import com.google.gson.Gson;
import com.inteligo.quiz.bank.beans.ATMBean;
import com.inteligo.quiz.bank.beans.ATMGeneralBean;
import com.inteligo.quiz.bank.beans.IData;
import com.inteligo.quiz.bank.beans.ResponseATM;
import com.inteligo.quiz.bank.model.Change;
import com.inteligo.quiz.bank.repository.ChangeRepository;
import com.inteligo.quiz.bank.resttemplete.RestTemplateService;
import com.inteligo.quiz.bank.resttemplete.RestTemplateServiceImpl;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/atm")
public class ATMController {


    @GetMapping("/getATM")
    public @ResponseBody
    ResponseEntity<?> getATMlist() throws IOException, JSONException, UnirestException {
        ResponseATM rpta = new ResponseATM();
        rpta.setHttpStatusCode(200);
        rpta.setCode(200);
        rpta.setDescription("Success");

        RestTemplateService restTemplateService = new RestTemplateServiceImpl();
        Map<String, String> parameters = new HashMap<>();

        String responseApi = restTemplateService.invokeGeneralServiceAPI("https://www.ing.nl/api/locator/atms/", HttpMethod.GET,parameters);
        //Parse Gson
        Gson gson = new Gson();
        //Eliminando lineas erróneas en el JSON obtenido
        responseApi= responseApi.replace(")]}',","");
        //Viene como arreglo y da error, formateando para que llegue como un objeto
        String strJsonApi="{\"atmbean\":"+responseApi+"}";

        ATMGeneralBean atmBean= gson.fromJson(strJsonApi, ATMGeneralBean.class);

        rpta.setData(atmBean);

        System.out.println("# de arreglos encontrados: " + atmBean.getAtmbean().size());
        //HttpHeaders responseHeaders = new HttpHeaders();

        return new ResponseEntity<ResponseATM>(rpta, HttpStatus.valueOf(rpta.getHttpStatusCode()));

    }






}
