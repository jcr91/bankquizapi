package com.inteligo.quiz.bank.controller;


import com.google.gson.Gson;
import com.inteligo.quiz.bank.beans.*;
import com.inteligo.quiz.bank.model.Change;
import com.inteligo.quiz.bank.resttemplete.RestTemplateService;
import com.inteligo.quiz.bank.resttemplete.RestTemplateServiceImpl;
import org.json.JSONException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/exchangerate")
public class ExchangeRateController {

    @GetMapping("/all")
    public @ResponseBody
    ResponseEntity<?> newChange() throws IOException, JSONException {

        ResponseATM rpta = new ResponseATM();


        rpta.setHttpStatusCode(HttpStatus.OK.value());
        rpta.setCode(HttpStatus.OK.value());
        rpta.setDescription("Success");


        RestTemplateService restTemplateService = new RestTemplateServiceImpl();
        Map<String, String> parameters = new HashMap<>();

        String responseApi = restTemplateService.invokeGeneralServiceAPI("http://api.currencylayer.com/live?access_key=377dd9059733f2c7e16b4dfc68b26435&format=1", HttpMethod.GET,parameters);
        //Parse Gson
        Gson gson = new Gson();

        ExchangeRate exchangeRate= gson.fromJson(responseApi, ExchangeRate.class);
        System.out.println(exchangeRate.getQuotes().size());

        for (Map.Entry me : exchangeRate.getQuotes().entrySet()) {
            System.out.println("Key: "+me.getKey() + " & Value: " + me.getValue());
        }

        rpta.setData(exchangeRate);

        return new ResponseEntity<ResponseATM>(rpta, HttpStatus.valueOf(rpta.getHttpStatusCode()));

    }
}
