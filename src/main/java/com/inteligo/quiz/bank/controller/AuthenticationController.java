package com.inteligo.quiz.bank.controller;

import com.inteligo.quiz.bank.beans.JwtResponse;
import com.inteligo.quiz.bank.beans.Login;
import com.inteligo.quiz.bank.model.Change;
import com.inteligo.quiz.bank.security.JwtProvider;
import com.inteligo.quiz.bank.beans.ResponseATM;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    JwtProvider jwtProvider;

    @PostMapping("/login")
    public @ResponseBody
    ResponseEntity<?> login(@RequestHeader Map<String, String> headers,@RequestBody Login login) throws IOException, JSONException, UnirestException {

        AtomicInteger validateHeader= new AtomicInteger();
        AtomicInteger validateHeaderUser= new AtomicInteger();
        AtomicInteger validateHeaderKey= new AtomicInteger();
    /*    headers.forEach((key, value) -> {

            if(key.equals("user") && value.equals("userbank") && (validateHeaderUser.get() == 0)){
                validateHeader.getAndIncrement();
                validateHeaderUser.getAndIncrement();
            }else if(key.equals("key") && value.equals("quiz12345") && validateHeaderKey.get() == 0){
                validateHeader.getAndIncrement();
                validateHeaderKey.getAndIncrement();
            }
        });*/


        Login l = new Login();
        l.setUser(login.getUser());
        l.setKey(login.getKey());

        System.out.println("-" + login.getUser());
        System.out.println("-" + login.getKey());
        System.out.println("-" + l.getUser());
        System.out.println("-" + l.getKey());
        System.out.println("-" + validateHeader);
        System.out.println("-" + validateHeaderUser);

        if(l.getUser().equals("userbank") && l.getKey().equals("quiz12345")){
            validateHeader.getAndIncrement();
            validateHeaderUser.getAndIncrement();
        }
        System.out.println("--" + validateHeader);
        System.out.println("--" + validateHeaderUser);

        UUID uuid = UUID.randomUUID();

        ResponseATM rpta = new ResponseATM();
        rpta.setHttpStatusCode(HttpStatus.OK.value());


        if(validateHeader.get() == 1){

            rpta.setCode(HttpStatus.OK.value());
            rpta.setDescription("Success");
            /*Generando Jwt*/
            JwtResponse jwtResponse = new JwtResponse();
            /*Aqui se puede almacenar el usuario y/o otro dato importante, para la demo solo un UUID random*/
            jwtResponse.setJwt(jwtProvider.generateJwtToken(uuid.toString()));

            rpta.setData(jwtResponse);

        }else{
            rpta.setCode(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.value());
            rpta.setDescription(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED.getReasonPhrase());
            rpta.setData(null);
        }


        return new ResponseEntity<ResponseATM>(rpta, HttpStatus.valueOf(rpta.getHttpStatusCode()));

    }



}
