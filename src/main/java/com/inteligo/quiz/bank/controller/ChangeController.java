package com.inteligo.quiz.bank.controller;

import com.inteligo.quiz.bank.beans.JwtResponse;
import com.inteligo.quiz.bank.beans.ResponseATM;
import com.inteligo.quiz.bank.model.Change;
import com.inteligo.quiz.bank.repository.ChangeRepository;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/change")
public class ChangeController {

    @Autowired
    ChangeRepository changeRepository;

    @PostMapping("/save")
    public @ResponseBody
    ResponseEntity<?> newChange(@RequestBody Change reqChange) {
        System.out.println(reqChange.toString());
        ResponseATM rpta = new ResponseATM();
        try {

            Change c = new Change();
            c.setIdClient(reqChange.getIdClient());
            c.setFromMoney(reqChange.getFromMoney());
            c.setToMoney(reqChange.getToMoney());
            c.setExchangeRate(reqChange.getExchangeRate());
            c.setMoney(reqChange.getMoney());
            c.setCreated(new Date());

            changeRepository.save(c);

            rpta.setHttpStatusCode(HttpStatus.OK.value());
            rpta.setCode(HttpStatus.OK.value());
            rpta.setDescription("Registro guardado correctamente.");
            rpta.setData(null);
        }catch (Exception e){

            rpta.setHttpStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            rpta.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            rpta.setDescription(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
            rpta.setData(null);
        }


        return new ResponseEntity<ResponseATM>(rpta, HttpStatus.valueOf(rpta.getHttpStatusCode()));

    }

}
