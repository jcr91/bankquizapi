package com.inteligo.quiz.bank.security;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.InvalidKeyException;
import io.jsonwebtoken.security.Keys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);
  
    @Value("${grokonez.app.jwtSecret}")
    private String jwtSecret;

    @Value("${grokonez.app.jwtExpiration}")
    private int jwtExpiration;
    
	public String generateJwtToken(String username){

		Collection<? extends GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

        return Jwts.builder().setId(UUID.randomUUID().toString())
        		.setSubject(username)
        		.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
        		.setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration))
        		.signWith(getSigningKey())
        		.compact();

    }
    
	private Key getSigningKey() {

		byte[] keyBytes = Decoders.BASE64.decode(jwtSecret);
		return Keys.hmacShaKeyFor(keyBytes);
	}
	
    public boolean validateJwtToken(String authToken) {
        boolean isTokenValid = false;
	    try {
            logger.info("Validando token: ");
            logger.info(authToken);
            Jwts.parser().setSigningKey(getSigningKey()).parseClaimsJws(authToken);
            isTokenValid = true;

        } catch ( InvalidKeyException e) {
            logger.error("Invalid JWT signature -> Message: {} ", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message: {}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message: {}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Message: {}", e);
        }
        logger.info("TOKEN VALUE: " + isTokenValid);
        return isTokenValid;
    }
    
    public Claims getUserNameFromJwtToken(String token) {
    	
    	return Jwts.parser().setSigningKey(getSigningKey()).parseClaimsJws(token).getBody();//.getSubject();
    	
    }
}