package com.inteligo.quiz.bank.repository;

import com.inteligo.quiz.bank.model.Change;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChangeRepository extends JpaRepository<Change, Integer> {

}
