package com.inteligo.quiz.bank.beans;

import java.util.HashMap;
import java.util.List;

public class GenericExchange implements IData {

    private List<String> quotes;

    public List<String> getQuotes() {
        return quotes;
    }

    public void setQuotes(List<String> quotes) {
        this.quotes = quotes;
    }
}
