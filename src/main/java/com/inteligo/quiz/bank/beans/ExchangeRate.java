package com.inteligo.quiz.bank.beans;

import java.util.HashMap;

public class ExchangeRate implements IData{

    private boolean success;
    private String terms;
    private String privacy;
    private String timestamp;
    private String source;
    private HashMap<String,String> quotes;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public HashMap<String, String> getQuotes() {
        return quotes;
    }

    public void setQuotes(HashMap<String, String> quotes) {
        this.quotes = quotes;
    }

    @Override
    public String toString() {
        return "ExchangeRate{" +
                "success=" + success +
                ", terms='" + terms + '\'' +
                ", privacy='" + privacy + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", source='" + source + '\'' +
                ", quotes=" + quotes +
                '}';
    }
}
