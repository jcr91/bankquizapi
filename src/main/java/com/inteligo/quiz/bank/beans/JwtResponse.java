package com.inteligo.quiz.bank.beans;

import java.io.Serializable;

public class JwtResponse implements Serializable, IData {

	private String jwt;

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

}