package com.inteligo.quiz.bank.beans;

public class Change {


    private String idClient;
    private String fromMoney;
    private String toMoney;
    private String exchangeRate;
    private String money;

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    public String getFromMoney() {
        return fromMoney;
    }

    public void setFromMoney(String fromMoney) {
        this.fromMoney = fromMoney;
    }

    public String getToMoney() {
        return toMoney;
    }

    public void setToMoney(String toMoney) {
        this.toMoney = toMoney;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
