package com.inteligo.quiz.bank.beans;

import java.util.List;

public class ATMGeneralBean implements IData{

    private List<ATMBean> atmbean;

    public List<ATMBean> getAtmbean() {
        return atmbean;
    }

    public void setAtmbean(List<ATMBean> atmbean) {
        this.atmbean = atmbean;
    }

    @Override
    public String toString() {
        return "ATMGeneralBean{" +
                "atmbean=" + atmbean +
                '}';
    }
}
