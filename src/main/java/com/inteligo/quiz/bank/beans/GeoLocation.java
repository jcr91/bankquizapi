package com.inteligo.quiz.bank.beans;

public class GeoLocation {

    private String lat;
    private String lgn;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLgn() {
        return lgn;
    }

    public void setLgn(String lgn) {
        this.lgn = lgn;
    }
}
