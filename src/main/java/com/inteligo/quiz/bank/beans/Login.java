package com.inteligo.quiz.bank.beans;

public class Login {

    private String user;
    private String key;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
