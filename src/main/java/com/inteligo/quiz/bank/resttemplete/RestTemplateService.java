package com.inteligo.quiz.bank.resttemplete;

import org.json.JSONException;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.util.Map;

public interface RestTemplateService {

    public <T> T invokeServiceAPI(String URL, HttpMethod METHOD, Map<String, String> parameters, Class<T> classOfT) throws JSONException, IOException;

    public String invokeGeneralServiceAPI(String URL, HttpMethod METHOD, Map<String, String> parameters) throws JSONException, IOException;

}

