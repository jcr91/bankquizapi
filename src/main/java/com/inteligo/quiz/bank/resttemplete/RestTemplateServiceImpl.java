package com.inteligo.quiz.bank.resttemplete;


import com.google.gson.Gson;
import com.google.gson.internal.Primitives;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

public class RestTemplateServiceImpl implements RestTemplateService {


    @Override
    public <T> T invokeServiceAPI(String URL, HttpMethod METHOD, Map<String, String> parameters, Class<T> classOfT) throws JSONException, IOException {
        Object object = null;
        System.out.println("ingreso service api");

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(URL);
        httpGet.setHeader("cache-control", "no-cache");
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpGet);
        String result = EntityUtils.toString(response.getEntity());
        client.close();
        result = result.replace(")]}',","");
        //System.out.println(result);

        System.out.println(result);
        Gson gson = new Gson();
        object = gson.fromJson(result, (Type) classOfT);

        return Primitives.wrap(classOfT).cast(object);
    }

    @Override
    public String invokeGeneralServiceAPI(String URL, HttpMethod METHOD, Map<String, String> parameters) throws JSONException, IOException {
        String apiURL =URL;

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(apiURL);
        httpGet.setHeader("cache-control", "no-cache");
        httpGet.setHeader("Accept", "application/json");
        httpGet.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpGet);
        String result = EntityUtils.toString(response.getEntity());
        client.close();

        return result;
    }
}
